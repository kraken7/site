var gulp = require('gulp');
var less = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');
var rename = require('gulp-rename');
var compass = require('gulp-compass');
var clean = require('gulp-clean');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var wiredep = require('gulp-wiredep');
var useref = require('gulp-useref');
var tinypng = require('gulp-tinypng');
var cache = require('gulp-cache');
var browserSync = require('browser-sync').create();

gulp.task('default', ['clean'], function() {
	gulp.run('dev');
});

gulp.task('production', ['clean'], function() {
	gulp.run('build');
});

gulp.task('dev', ['build', 'watch', 'browser-sync']);

gulp.task('build', ['html', 'styles', 'scripts', 'assets']);

gulp.task('watch', function() {
	gulp.watch('src/css/**/*.less', ['styles']);
    gulp.watch('src/js/**/*.js', ['scripts']);
    gulp.watch('src/index.html', function(){
    	gulp.src('src/index.html')
    	.pipe(wiredep({
			directory: 'bower_components/'
		}))
		.pipe(gulp.dest('build/'))
		.on('end', function() {
			gulp.run('vendoruseref');
		});
    });
    gulp.watch('src/assets/**/*.*', ['assets']);
    //gulp.watch('src/**/*.*', ['clearcache']);
    gulp.watch('src/**/*.*').on('change', function(e) {
	    return gulp.src(e.path)
	        .pipe(browserSync.reload({stream: true}));
	});
    //gulp.watch('src/**/*.*').on('change', browserSync.reload);
});

gulp.task('styles', function() {
	return gulp.src('src/css/*.less')
		.pipe(plumber({
			errorHandler: notify.onError(function(err) {
				return {
					title: 'Styles',
					message: err.message
				}
			})
		}))
		.pipe(sourcemaps.init())
		.pipe(less())
		.pipe(autoprefixer({
			browsers: ['last 2 versions']
		}))
		.pipe(concat('styles.min.css'))
		.pipe(cssnano())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('build/css'));
});

gulp.task('scripts', function() {
	gulp.src('src/js/*.js')
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('build/js'));
});

gulp.task('assets', function() {
	gulp.src('src/assets/**/*.*')
		.pipe(gulp.dest('build/assets'));
});

gulp.task('tiny', function() {
	gulp.src('build/assets/**/*.{png,jpg}')
		.pipe(tinypng('qrr-9BOGSeyr540SlwSMedbdzQFks4qW'))
		.pipe(gulp.dest('./build/assets'));
});

gulp.task('html', function() {
	gulp.src('src/index.html')
		.pipe(wiredep({
			directory: 'bower_components/'
		}))
		.pipe(gulp.dest('build/'))
		.on('end', function() {
			gulp.run('vendoruseref');
		});
});

gulp.task('useref', function() {
	return gulp.src('build/index.html')
		.pipe(useref())
		.pipe(gulp.dest('build/'));
});

gulp.task('vendoruseref', function() {
	return gulp.src('build/index.html')
		.pipe(useref())
		.pipe(gulp.dest('build/'))
		.on('end', function() {
			gulp.run(['vendorcss', 'vendorjs']);
		});
});

gulp.task('vendorcss', function() {
	return gulp.src('build/vendor/vendor.css')
		.pipe(cssnano())
		.pipe(gulp.dest('build/vendor'));
});

gulp.task('vendorjs', function() {
	gulp.src('build/vendor/vendor.js')
		.pipe(uglify())
		.pipe(gulp.dest('build/vendor'));
});

gulp.task('clean', function() {
	return gulp.src('build/')
		.pipe(clean());
});

gulp.task('clearcache', function() {
	gulp.src('./src/**/*.*')
  		.pipe(cache.clear());
	cache.clearAll();

});

gulp.task('browser-sync', function() {
	return browserSync.init({
		server: {
			baseDir: './build/'
		}
	});
});