$(document).ready(function(){

	/* ТАЙМЕР */
	(function(){
		time = $('.header__timer .header__timer-num');
		day = time.eq(0).text();
		hour = time.eq(1).text();
		min = time.eq(2).text();
		sec = time.eq(3).text();

		timer = setInterval(function(){
			sec--;
			if(sec < 0){
				sec = 59;
				min--;
				if(min < 0){
					min = 59;
					hour--;
					if(hour < 0){
						hour = 23;
						day--;
						if(day < 0){
							day = 0;
							hour = 0;
							min = 0;
							sec = 0;
							clearInterval(timer);
						}
						if(day < 10) day = '0' + day;
						time.eq(0).text(day);
					}
					if(hour < 10) hour = '0' + hour;
					time.eq(1).text(hour);
				}
				if(min < 10) min = '0' + min;
				time.eq(2).text(min);
			}
			if(sec < 10) sec = '0' + sec;
			time.eq(3).text(sec);

		}, 1000);
	}());
	/* ТАЙМЕР */

	/* НАВИГАЦИЯ */
	(function(){
		$('.nav-link').click(function(e){
			e.preventDefault();
			href = $(this).attr('href');
			destination = $(href).offset().top;
			$("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
			return false;
		});
	}());
	/* НАВИГАЦИЯ */

	/* ТАБЛИЦА */
	(function(){
		$('.ico-plus').click(function(e){
			e.preventDefault();
			$(this).parents('tr').next('tr').find('div').slideToggle('500');
		});
	}());
	/* ТАБЛИЦА */

	/* МЕНЮ ГАМБУРГЕР */
	(function(){
		$('.header__top--mobil-burger').click(function(){
			$('.header__top--mobil-nav-list').slideToggle(500);
		});
	}());
	/* МЕНЮ ГАМБУРГЕР */

});

/* НАВ МЕНЮ ПРИ СКРОЛЛЕ */
$(document).scroll(function(){
	if($(this).scrollTop() > 300){
		$('.header__top.header__top--scroll').css({'top':0});
	}
	else{
		$('.header__top.header__top--scroll').css({'top':'-200px'});
	}

	if($(this).scrollTop() > 50){
		$('.header__top--mobil').addClass('scroll');
	}
	else{
		$('.header__top--mobil').removeClass('scroll');
	}
});
/* НАВ МЕНЮ ПРИ СКРОЛЛЕ */